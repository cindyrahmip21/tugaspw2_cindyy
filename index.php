<?php
 include('koneksi.php'); //agar index terhubung dengan database, maka koneksi sebagai penghubung harus di include
?>
<!DOCTYPE html>
<html>
 <head>
 <title>DATA MAHASISWA</title>
 <style type="text/css">
 * {
 font-family: "Trebuchet MS";
 }
 h1 {
 text-transform: uppercase;
 color: salmon;
 }
 table {
 border: solid 1px #DDEEEE;
 border-collapse: collapse;
 border-spacing: 0;
 width: 90%;
 margin: 10px auto 10px auto;
 }
 table thead th {
 background-color: #DDEFEF;
 border: solid 1px #DDEEEE;
 color: #336B6B;
 padding: 10px;
 text-align: left;
 text-shadow: 1px 1px 1px #fff;
 text-decoration: none;
 }
 table tbody td {
 border: solid 1px #DDEEEE;
 color: #333;
 padding: 10px;
 text-shadow: 1px 1px 1px #fff;
 }
 a {
 background-color: salmon;
 color: #fff;
 padding: 10px;
 text-decoration: none;
 font-size: 12px;
 }
 </style>
 </head>
 <body>
 <center><h1>Data Mahasiswa</h1><center>
 <center><a href="tambah_mahasiswa.php">+ &nbsp; Tambah Data</a><center>
 <br/>
 <table>
 <thead>
 <tr>
 <th>No</th>
 <th>Nim</th>
 <th>Nama Mahasiswa</th>
 <th>Jenis Kelamin</th>
 <th>Jurusan</th>
 <th>Alamat</th> 
 <th>Action</th>
 </tr>
 </thead>
 <tbody>
 <?php
 // jalankan query untuk menampilkan semua data diurutkan berdasarkan nim
 $query = "SELECT * FROM tb_mahasiswa ORDER BY nim ASC";
 $result = mysqli_query($koneksi, $query);
 //mengecek apakah ada error ketika menjalankan query
 if(!$result){
    die ("Query Error: ".mysqli_errno($koneksi).
    " - ".mysqli_error($koneksi));
    }
    //buat perulangan untuk element tabel dari data mahasiswa
    $no = 1; //variabel untuk membuat nomor urut
    // hasil query akan disimpan dalam variabel $data dalam bentuk array
    // kemudian dicetak dengan perulangan while
    while($row = mysqli_fetch_assoc($result))
    {
    ?>
    <tr>
    <td><?php echo $no; ?></td>
    <td><?php echo $row['nim']; ?></td>
    <td><?php echo $row['nama']; ?></td>
    <td><?php echo $row['jenis_kelamin']; ?></td>
    <td><?php echo $row['jurusan']; ?></td>
    <td><?php echo $row['alamat']; ?></td>
    <td>
    <a href="edit_mahasiswa.php?nim=<?php echo $row['nim']; ?>">Edit</a>  | 
    <a href="hapus_mahasiswa.php?nim=<?php echo $row['nim']; ?>" onclick="return
   confirm('Anda yakin akan menghapus data ini?')">Hapus</a>
    </td>
    </tr>
    <?php
    $no++; //untuk nomor urut terus bertambah 1
    }
    ?>
    </tbody>
    </table>
    </body>
   </html>
   





<body>
<center><h1>Data Mata Kuliah</h1><center>
<center><a href="tambah_matkul.php">+ &nbsp; Tambah Matkul</a><center>
 <br/>
 <table>
 <thead>
 <tr>
 <th>No</th>
 <th>Kode Matkul</th>
 <th>Nama Matkul</th>
 <th>Jumlah SKS</th>
 <th>Dosen</th>
 <th>Action</th>
 </tr>
 </thead>
 <tbody>
 <?php
 // jalankan query untuk menampilkan semua data diurutkan berdasarkan nim
 $query = "SELECT * FROM tb_matkul ORDER BY kode_matkul ASC";
 $result = mysqli_query($koneksi, $query);
 //mengecek apakah ada error ketika menjalankan query
 if(!$result){
    die ("Query Error: ".mysqli_errno($koneksi).
    " - ".mysqli_error($koneksi));
    }
    //buat perulangan untuk element tabel dari data mahasiswa
    $no = 1; //variabel untuk membuat nomor urut
    // hasil query akan disimpan dalam variabel $data dalam bentuk array
    // kemudian dicetak dengan perulangan while
    while($row = mysqli_fetch_assoc($result))
    {
    ?>
    <tr>
    <td><?php echo $no; ?></td>
    <td><?php echo $row['kode_matkul']; ?></td>
    <td><?php echo $row['nama_matkul']; ?></td>
    <td><?php echo $row['jml_sks']; ?></td>
    <td><?php echo $row['dosen']; ?></td>
    <td>
    <a href="edit_matkul.php?kode_matkul=<?php echo $row['kode_matkul']; ?>">Edit</a> | 
    <a href="hapus_matkul.php?kode_matkul=<?php echo $row['kode_matkul']; ?>" onclick="return
   confirm('Anda yakin akan menghapus data ini?')">Hapus</a>
    </td>
    </tr>
   
    <?php
    $no++; //untuk nomor urut terus bertambah 1
    }
    ?>
    </tbody>
    </table>
    </body>
   </html>